-- License CC0 (Creative Commons license) (c) darkfrei, 2021

local fov = require ('fov')

function lovr.load()
	--local ww, wh = lovr.window.getDesktopDimensions( displayindex )
	--lovr.window.setMode( ww/2, wh/2)
	
	--width, height = lovr.graphics.getDimensions( )

	map = {}
	rez = 16
	--max_i, max_j = math.floor(width/rez), math.floor(height/rez)
	max_i, max_j = 100, 100
	for i = 1, max_i do
		map[i] = {}
		for j = 1, max_j do
			if math.random (10) == 1 then
				map[i][j] = 0 -- 0 is wall, black
			else
				map[i][j] = 1 -- 1 is space, white
			end
		end
	end
	
	player = {i=math.floor(max_i/2), j=math.floor(max_j/2)}
	radius = 30
end

 
function lovr.update(dt)
	
end


function lovr.draw()
lovr.graphics.transform(-30,-10,-20, 0.05, 0.05, 0.05,0,0,0,1)
	-- draw map
	for i, js in pairs (map) do
		for j, value in pairs (js) do
			local c = (value+1)/3
			lovr.graphics.setColor (c,c,c)
			if value==0 then lovr.graphics.box('fill', rez*(i-1), rez*(j-1),0, rez, rez,180)
			else if value==1 then lovr.graphics.box('fill', rez*(i-1), rez*(j-1),0, rez, rez,60)
			end
			end
		end
	end
	
	-- get and draw fov
	local view = fov.marching (map, player.i, player.j, radius)
	for i, js in pairs (view) do
		for j, value in pairs (js) do
			local c = value and 1 or 0
			lovr.graphics.setColor (c,c,1-c)
			--lovr.graphics.box('fill', rez*(i-1), rez*(j-1),0, rez, rez,120)
					if c==0 then lovr.graphics.box('fill', rez*(i-1), rez*(j-1),0, rez, rez,120)
			elseif c==1 then lovr.graphics.box('fill', rez*(i-1), rez*(j-1),0, rez, rez,10)
			end
			if map[i] and map[i][j] then
				map[i][j] = value and 2 or -1
			end
		end
	end
	
	-- draw player
	lovr.graphics.setColor (0,1,0)
	lovr.graphics.box('fill', rez*(player.i-1), rez*(player.j-1),0, rez, rez,0)
	
	-- GUI
	lovr.graphics.setColor (1,1,1)
	  lovr.graphics.print("Move Camera with -WASD-", 500,1530,300,100,45,1,0,0)
	  lovr.graphics.print("move dot with -TFGH- ", 500,1530,200,100,45,1,0,0)
	--lovr.graphics.print('press WASD to move green dot')
end

function lovr.keypressed(key, scancode, isrepeat)
	--if key == "d" then
		--player.i = player.i + 1
	--elseif key == "s" then
		--player.j = player.j + 1
	--elseif key == "a" then
		--player.i = player.i - 1
	--elseif key == "w" then
		--player.j = player.j - 1
		
		if key == "t" then
		player.i = player.i + 1
	elseif key == "g" then
		player.j = player.j + 1
	elseif key == "f" then
		player.i = player.i - 1
	elseif key == "h" then
		player.j = player.j - 1
	elseif key == "escape" then
		lovr.event.quit()
	end
end
