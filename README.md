# love2d-lua-tests VR ports

This is a fork of the love2d repository: https://github.com/darkfrei/love2d-lua-tests

You will find here some of the algorithms initially created for love2d ported to vr.

![Screenshot 1](/screenshots/fieldofviewvr.png)
![Screenshot 1](/screenshots/flappybirdvr.png)
![Screenshot 1](/screenshots/fouriertransform2.gif)
![Screenshot 1](/screenshots/metaballs2.gif)
![Screenshot 1](/screenshots/particlecollisions.png)
![Screenshot 1](/screenshots/pathfindingone.png)
![Screenshot 1](/screenshots/pushblocksvr.png)

For more information please check the wiki: https://codeberg.org/glitchapp/love2d-lua-vr-ports/wiki/Home