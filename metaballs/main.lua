-- License CC0 (Creative Commons license) (c) darkfrei, 2021

title = "Metaballs"
--sms = require ('sms')

function lovr.load()
	--lovr.window.setTitle( title )
	--local ww, wh = lovr.window.getDesktopDimensions()
	--lovr.window.setMode( ww/2, wh/2)
	local ww,wh = 800, 600 
	width, height = 800,600

	rez = 16
	map_width, map_height = math.floor((width-rez)/rez), math.floor((height-rez)/rez)
	
	blobs = {}
	for n = 1, 30 do
		local blob = {x=math.random (map_width), y=math.random(map_height)}
		blob.radius = math.random(10, 100)
		blob.rezradius = blob.radius / 32
		blob.vx = -math.random (50, 200)/rez
		blob.vy = -math.random (10, 200)/rez
--		blob.color = ({{1,0,0},{0,1,0},{0,0,1}})[math.random(3)]
		blob.color = ({{1,0,0},{0,1,0},{0,0,1}})[(n-1)%3+1]
		table.insert (blobs, blob)
	end
end

 
function lovr.update(dt)
	for n, blob in pairs (blobs) do
		blob.x = blob.x +dt*blob.vx
		blob.y = blob.y +dt*blob.vy
		if blob.x < blob.rezradius+0.5 then 
			blob.x = blob.rezradius+0.5
			blob.vx = -blob.vx
		elseif blob.x > map_width-blob.rezradius-0.5 then
			blob.x = map_width-blob.rezradius-0.5
			blob.vx = -blob.vx
		end
		
		if blob.y < blob.rezradius+0.5 then 
			blob.y = blob.rezradius+0.5
			blob.vy = -blob.vy
		elseif blob.y > map_height-blob.rezradius-0.5 then
			blob.y = map_height-blob.rezradius-0.5
			blob.vy = -blob.vy
		end
		
	end
end


function dist(x1, y1, x2, y2)
	return math.sqrt((x2-x1)^2+(y2-y1)^2)
end

function lovr.draw()
lovr.graphics.transform(-20,-3,-30, 0.05, 0.05, 0.05)
	for i = 1, map_width do
		for j = 1, map_height do
--			local c = {0,0,0}
			local r, g ,b = 0,0,0
			for n, blob in pairs (blobs) do
				local d = dist(i, j, blob.x, blob.y)
				r = r + blob.color[1]*(1/(2*rez))*blob.radius/d
				g = g + blob.color[2]*(1/(2*rez))*blob.radius/d
				b = b + blob.color[3]*(1/(2*rez))*blob.radius/d
				
			end
			lovr.graphics.setColor(r*r,g*g,b*b)
			lovr.graphics.box('fill', rez*(i-1/2),0, rez*(j-1/2), rez, rez,0,100,0,0,0,0)
		end
	end
	lovr.graphics.setColor(0,0,1)
	--lovr.graphics.box('line', 0.5*rez, 0.5*rez,0, rez*map_width, rez*map_height,100,0,0,0,0)
	
--	for n, blob in pairs (blobs) do
--		lovr.graphics.circle('line', blob.x*rez, blob.y*rez, blob.radius)
--	end
end

function lovr.keypressed(key, scancode, isrepeat)
	if key == "escape" then
		lovr.event.quit()
	end
end
